# dpub_ch_fb

This programm should transform the 3d image sinus.raw into 3 2d images. centor along the corresponding axis.

The dataset is a volume where one **voxel** is stored as a little endian - 16 uint. The lower 12 bits represents a HU unit from 0 - 4095.
These units must be parsed as 8-bit integers (uint8) and store them as a picture.

## HU unit

[HU color](https://www.sciencedirect.com/topics/medicine-and-dentistry/hounsfield-scale)

1000 = white
0 = black 

## Index order of voxel .raw file

[Indexing](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Fslutils)

Try rendering with: fsl2ascii
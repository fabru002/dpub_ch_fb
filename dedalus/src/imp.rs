// ===================================================  Imports =================================================== //
use byteorder::{LittleEndian, ReadBytesExt};
use image;
use serde::Deserialize;
use std::{
    convert::TryInto,
    error::Error,
    fs,
    fs::File,
    io::{self, BufRead, Cursor},
};

// ===================================================  Structure ================================================= //
/// This struct defines the main application
///
/// # Attributes
/// * data (u8): the 3d image buffered
/// * out_data (u8): holds the transformed output data
/// * dimension (usize): represents the dimension. Red from the header file .mdh. Must follow the example structure
#[derive(Debug, Deserialize)]
pub struct App {
    pub data: Vec<u8>,
    pub out_data: Vec<u8>,
    pub dimension: Vec<u16>,
}

// Implement methods to App
impl App {
    /// Constuctor: reads in data file and stores data to app.data
    pub fn new(path: &str, path_header: &str) -> Result<Self, Box<dyn Error>> {
        // access raw file and store as buffer
        let data = fs::read(path)?;

        // get dimension from header
        let mut dimension: Vec<u16> = Vec::new();

        // Read in lineslines
        let header_lines = io::BufReader::new(File::open(path_header)?).lines();

        for (index, line) in header_lines.enumerate() {
            // Only handle the second line
            if index == 1 {
                if let Ok(content) = line {
                    // Handle line
                    let dim_vec: Vec<&str> = content.split(" ").collect();

                    // push into dimension
                    dimension.push(dim_vec[2].parse::<u16>()?);
                    dimension.push(dim_vec[3].parse::<u16>()?);
                    dimension.push(dim_vec[4].parse::<u16>()?);
                }
            }
        }

        Ok(Self {
            data: data,
            out_data: Vec::new(),
            dimension: dimension,
        })
    }

    /// Checks if the dataset is a multiple of 16-bits, since 1 voxel = 16-Bit. If the result is a f32, then the dataset is currupt
    pub fn check_dataset(&mut self) -> bool {
        // Check if the data set is dividable by 16 without rest
        if self.data.len() % 16 == 0 {
            return true;
        } else {
            return false;
        }
    }

    /// Transform the 16-bit uint to uint8
    pub fn transform_data(&mut self) -> Result<(), Box<dyn Error>> {
        // Iterate through raw data and store the HU as 8uint
        for i in 0..self.data.len() {
            if i % 2 == 0 {
                let mut buffer_2 = Cursor::new(vec![self.data[i], self.data[i + 1]]);
                let hu = buffer_2.read_u16::<LittleEndian>()?;

                // map 0-4096 to 0-255
                // out_data should be half of the len of data
                let hu_8bit: usize = ((hu as usize * 255) as usize / 4095).try_into()?;

                // push new HU values to out_data
                self.out_data.push(hu_8bit as u8);
            }
        }

        // fast debug
        println!(
            "Data could be transformed. Length of the data: {:?}",
            self.out_data.len()
        );

        // debug the max value
        let max = self.out_data.iter().max();
        println!("Max of u8 values: {:?}", max);

        Ok(())
    }

    /// Helper to calculate Adress in buffer
    fn healper_adress(&mut self, x: u16, y: u16, z: u16 ) -> Result<usize, Box<dyn Error>> {
        return Ok((x as usize * (self.dimension[1] as usize + 1) + y as usize) * (self.dimension[2] as usize + 1) + z as usize)
    }

    /// Create x-axis central image
    pub fn render_x_axis(&mut self, path: &str) -> Result<(), Box<dyn Error>> {
        // dx = 512
        // dy = 512
        // dz = 333

        // Dimension for output PNG: 512 x 333

        // Implement algorithm to count through the main data and get correct values
        // x = is static on half = 256

        // Formula:                                                
        // Address of[i][j][k] = {[(I – LR) * N] + [(J – LC)]} * R + [K – LB]
        // I = i
        // LR = low limit of dimension[0] = 0
        // LC = low limit of dimension[1] = 0
        // LB = low limit of dimension[2] = 0
        // N = nr cloumns (y) = 512 -0 +1
        // R = nr blocks (z) = 333 -0 +1

        // => Address of[x][y][z] = {[x * N] + y} * R + z

        // main buffer. Init with fixed capacity
        let mut buffer_main: Vec<u8> =
            Vec::with_capacity(self.dimension[1] as usize * self.dimension[2] as usize);

        // get all the y value from buffer and store them aacording to their value as triplets to buffer
        for y in 0..(self.dimension[1]) {
            // get all the z values from buffer after the first y series
            for z in 0..(self.dimension[2]) {
                // get index
                let index = self.healper_adress(266, y, z)?;

                // push three time the same value
                for _j in 0..3 {
                    buffer_main.push(self.out_data[index]);
                }
            }
        }

        // write the image
        // buffer syntax: [v1r, v1g, v1y, v2r, v2g, v2y,  ......  ]
        image::save_buffer(
            path,
            &buffer_main,
            self.dimension[1] as u32,
            self.dimension[2] as u32,
            image::ColorType::Rgb8,
        )?;

        Ok(())
    }

    /// Create x-axis central image
    pub fn render_y_axis(&mut self, path: &str) -> Result<(), Box<dyn Error>> {
        // Map the values for the center of y-axis into buffer
        let buffer: &[u8] = &[122; 27];

        // write the image
        image::save_buffer(path, buffer, 3, 3, image::ColorType::Rgb8)?;

        Ok(())
    }

    /// Create x-axis central image
    pub fn render_z_axis(&mut self, path: &str) -> Result<(), Box<dyn Error>> {
        // Map the values for the center of z-axis into buffer
        let buffer: &[u8] = &[122; 27];

        // write the image
        image::save_buffer(path, buffer, 3, 3, image::ColorType::Rgb8)?;

        Ok(())
    }
}

// ===================================================  Testing  ================================================= //
#[cfg(test)]
mod tests {
    use super::*;

    // Test if init function new is failing if file is not found
    #[test]
    fn test_init() {
        assert!(App::new("unvalid_path_to_file.raw", "unvalid_path_to_file.mhd").is_err());
    }

    #[test]
    fn test_check_dataset() {
        // Create dummy App
        let mut app = App {
            data: vec![2; 16],
            out_data: vec![2; 2],
            dimension: vec![2; 2],
        };

        assert_eq!(app.check_dataset(), true);

        app.data.push(3);

        assert_eq!(app.check_dataset(), false);
    }
}

//! Dedalus challange: 3d to 6 2ds

// ===================================================  Imports =================================================== //
use std::time::{SystemTime};
mod imp;

// ===================================================  Globals =================================================== //

// make the path available via args
static PATH_RAW: &str = "./sinus.raw";
static PATH_HED: &str = "./sinus.mhd";

// ===================================================  Main     ================================================= //
fn main() {
    // debug time
    let now = SystemTime::now();

    // Init app
    let mut app = match imp::App::new(PATH_RAW, PATH_HED) {
        Ok(app) => app,
        Err(e) => panic!("{:?}", e),
    };

    // Check the dataset
    if !app.check_dataset() {
        panic!("Dataset is currupt!")
    }

    // Debug dimension
    println!("Dimesnion {:?}", app.dimension);

    // Transform
    app.transform_data().unwrap();

    // Rendering
    app.render_x_axis("./x_axis.png").unwrap();

    // debug time
    let finish = SystemTime::now();
    let runtime = finish
        .duration_since(now)
        .expect("Clock may have gone backwards");
    println!("Runtime {:?}", runtime);

    // Clean exit
    std::process::exit(0);
}
